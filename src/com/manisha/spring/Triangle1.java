package com.manisha.spring;

public class Triangle1 {
	Point pointA;
	Point pointB;
	Point pointC;
	public Point getPointA() {
		return pointA;
	}
	public void setPointA(Point pointA) {
		this.pointA = pointA;
	}
	public Point getPointB() {
		return pointB;
	}
	public void setPointB(Point pointB) {
		this.pointB = pointB;
	}
	public Point getPointC() {
		return pointC;
	}
	public void setPointC(Point pointC) {
		this.pointC = pointC;
	}
	void draw()
	{
		System.out.println("calling pointA: "+getPointA().getX()+":"+getPointA().getY());
		System.out.println("calling pointB "+getPointB().getX()+":"+getPointB().getY());
		System.out.println("calling pointC "+getPointC().getX()+":"+getPointC().getY());
	}

}
