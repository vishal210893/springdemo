package com.manisha.spring;

public class Triangle {
	
	int height;
	String type;
	
	public Triangle() {
		
	}

	
	public Triangle(int height) {
		super();
		this.height = height;
	}

	public Triangle(String type) {
		super();
		this.type = type;
	}

	public Triangle(String type, int height) {
		super();
		this.type = type;
		this.height = height;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	void draw() {
		System.out.println("triangle drawn="+getType()+":"+getHeight());
	}

}
