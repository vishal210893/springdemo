package com.manisha.spring;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.FileSystemResource;

public class Drawing {
	public static void main(String args[])
	{
		//BeanFactory bf=new XmlBeanFactory(new FileSystemResource("spring.xml"));
	/*	ApplicationContext app=new ClassPathXmlApplicationContext("spring.xml");
		Triangle triangle=(Triangle)app.getBean("triangle");
		triangle.draw();*/
		
/*		ApplicationContext app = new ClassPathXmlApplicationContext("spring1.xml");
		Triangle1 triangle = (Triangle1) app.getBean("triangle");
		triangle.draw();*/
		
		/*ApplicationContext app = new ClassPathXmlApplicationContext("spring2.xml");
		Triangle1 triangle = (Triangle1) app.getBean("triangle");
		triangle.draw();*/
		/*ApplicationContext app = new ClassPathXmlApplicationContext("spring3.xml");
		Triangle2 triangle = (Triangle2) app.getBean("triangle");
		triangle.draw();
		*/
		
		ApplicationContext app = new ClassPathXmlApplicationContext("spring4.xml");//autowire
		Triangle1 triangle = (Triangle1) app.getBean("triangle");
		triangle.draw();
		
		
	}
	
	
	

}
